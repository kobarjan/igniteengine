//
// Created by kobarjan on 17.04.2018.
//

#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include "ResourcesManager.h"
#include "Window.h"
#include "Loader.h"
#include "Renderer.h"

namespace IgniteEngine{
    class RenderEngineComponent {
    public:
        bool init();
        void loop();
        void cleanUp();
    private:
		ResourcesManager *resManager;
		Loader *loader;
        Window *window;
		Renderer *renderer;
    };
}

