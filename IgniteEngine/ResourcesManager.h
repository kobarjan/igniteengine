#pragma once

#include <string>

namespace IgniteEngine {
	class ResourcesManager
	{
	public:
		ResourcesManager();
		~ResourcesManager();
		void detectResources();
		void loadManagerFile();
		void loadSceneOrder();
		std::string loadWindowConfig();
		
	};
}

