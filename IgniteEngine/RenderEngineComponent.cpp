//
// Created by kobarjan on 17.04.2018.
//

#include "RenderEngineComponent.h"


bool IgniteEngine::RenderEngineComponent::init() {
	resManager = new ResourcesManager();

    window = new Window(1280,720,"xd");
	if (!window->init()) {
		return false;
	}
	return true;
}
void IgniteEngine::RenderEngineComponent::loop(){
    while (window->running()){
        window->update();
    }
}
void IgniteEngine::RenderEngineComponent::cleanUp() {
    window->cleanUp();
	delete resManager;
	delete window;
}