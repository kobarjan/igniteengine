#include <iostream>
#include <thread>
#include "RenderEngineComponent.h"

void runRenderingLoop() {
	IgniteEngine::RenderEngineComponent *renderEngineComponent = new IgniteEngine::RenderEngineComponent();
	renderEngineComponent->init();
	renderEngineComponent->loop();
	renderEngineComponent->cleanUp();
}
void runGamePlayLoop() {

}	
int main() {
    std::thread renderingLoop(runRenderingLoop);
	std::thread gameplayLoop(runGamePlayLoop);
	renderingLoop.join();
	gameplayLoop.join();
    return 0;
}