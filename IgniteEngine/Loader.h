#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include "Shader.h"

namespace IgniteEngine {
	class Loader
	{
	public:
		Loader();
		~Loader();
		int loadToVAO(float verts);
		Shader loadShader(std::string pathVert, std::string pathFrag);
		void loadScene();
		void loadOBJ();
		void loadPNG();
		void loadGLSL();
	private:
		std::string loadFile(std::string path);
	};
}


