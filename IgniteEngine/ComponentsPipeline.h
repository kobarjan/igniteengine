//
// Created by kobarjan on 22.04.2018.
//

#ifndef IGNITEENGINE_COMPONENTSPIPELINE_H
#define IGNITEENGINE_COMPONENTSPIPELINE_H

#include <vector>

namespace IgniteEngine {
    class ComponentsPipeline {
    public:
        static std::vector<int> getRenderMessages();
        static std::vector<int> getSoundMessages();
        static void pushMessage();
    private:
        static std::vector<int> messages;
    };
}

#endif //IGNITEENGINE_COMPONENTSPIPELINE_H
