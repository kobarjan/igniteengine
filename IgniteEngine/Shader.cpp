#include "Shader.h"

IgniteEngine::Shader::Shader(int id)
{
	ID = id;
}


IgniteEngine::Shader::~Shader()
{
}

void IgniteEngine::Shader::use() {
	glUseProgram(ID);
}
