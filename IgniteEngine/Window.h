//
// Created by kobarjan on 19.04.2018.
//

#ifndef IGNITEENGINE_WINDOW_H
#define IGNITEENGINE_WINDOW_H

#include <glad/glad.h>
#include <iostream>
#include <GLFW/glfw3.h>

namespace IgniteEngine {

    class Window {
    public:
        Window(int width, int height,const char* title);
        bool init();
        void update();
        void cleanUp();
        bool running();
    private:
        void frameSizeCallback(GLFWwindow* window, int width, int height);
        int width,height;
        const char* title;
        GLFWwindow* window;
    };
}


#endif //IGNITEENGINE_WINDOW_H
