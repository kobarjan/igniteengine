#pragma once

#include <vector>
#include "Object.h"
namespace IgniteEngine {
	class Renderer
	{
	public:
		Renderer();
		~Renderer();
		void render();
	private:
		std::vector<Object> obiekty;
	};
}


