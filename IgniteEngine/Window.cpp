//
// Created by kobarjan on 19.04.2018.
//


#include "Window.h"

IgniteEngine::Window::Window(int width, int height,const char* title) {
    this->width=width;
    this->height=height;
    this->title=title;
}

bool IgniteEngine::Window::init() {
    if(!glfwInit()){
        return false;
    }
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    window = glfwCreateWindow(width,height,title,NULL,NULL);
    glfwMakeContextCurrent(window);
    //glfwSetWindowSizeCallback(window,frameSizeCallback);
	
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return false;
    }
	glViewport(0, 0, width, height);
    return true;
}
void IgniteEngine::Window::update() {
    glfwPollEvents();
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glfwSwapBuffers(window);
}
void IgniteEngine::Window::cleanUp(){
    glfwDestroyWindow(window);
    glfwTerminate();
}
bool IgniteEngine::Window::running() {
    if(glfwWindowShouldClose(window))
        return false;
    else
        return true;
}
void IgniteEngine::Window::frameSizeCallback(GLFWwindow *window, int width, int height) {
    glViewport(0,0,width,height);
}
