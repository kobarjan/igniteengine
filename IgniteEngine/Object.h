#pragma once
#include <vector>
#include <glm\glm.hpp>
#include "Mesh.h"
namespace IgniteEngine {
	class Object
	{
	public:
		Object();
		~Object();
		void render();
		glm::vec3 position;
		glm::vec3 rotation;
		glm::vec3 scale;
	private:
		std::vector<Mesh> modele;
	
	};
}

