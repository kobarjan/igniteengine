#include "Loader.h"

IgniteEngine::Loader::Loader()
{
	
}	


IgniteEngine::Loader::~Loader()
{
}

IgniteEngine::Shader IgniteEngine::Loader::loadShader(std::string pathVert, std::string pathFrag) {
	const char *contentVert = loadFile(pathVert).c_str();
	const char *contentFrag = loadFile(pathFrag).c_str();
	int vertShader = glCreateShader(GL_VERTEX_SHADER);
	int fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	int program = glCreateProgram();
	glShaderSource(vertShader, 1, &contentVert, NULL);
	glShaderSource(fragShader, 1, &contentFrag, NULL);
	glCompileShader(vertShader);
	glCompileShader(fragShader);
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	glLinkProgram(program);
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
	Shader shader(program);
	return shader;
}
std::string IgniteEngine::Loader::loadFile(std::string path) {
	std::ifstream file;
	file.open(path);
	std::stringstream fileStream;

	fileStream << file.rdbuf();

	file.close();
	return fileStream.str();
}
