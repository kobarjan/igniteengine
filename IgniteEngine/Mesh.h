#pragma once

#include <vector>

namespace IgniteEngine {
	struct Texture
	{
		int id;
	};
	class Mesh
	{
	public:
		Mesh(int shaderID);
		~Mesh();
		void render();
		std::vector<float> verts;
		std::vector<float> nVerts;
		std::vector<float> tVerts;
		std::vector<unsigned int> indices;
		std::vector<Texture> textures;
	private:
		void setupMesh();
		int vaoID();
		int shaderID;
	};
}

