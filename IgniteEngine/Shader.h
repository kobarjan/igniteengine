#pragma once

#include <string>
#include <glm\glm.hpp>
#include <glad\glad.h>
#include <GLFW\glfw3.h>

namespace IgniteEngine {
	class Shader
	{
	public:
		Shader(int id);
		~Shader();
		void use();
		void setMat4(std::string name, glm::mat4 value);
		int ID;
	};
}


